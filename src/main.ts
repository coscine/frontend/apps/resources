import jQuery from "jquery";
import Vue from "vue";
import ResourcesApp from "./ResourcesApp.vue";
import VueI18n from "vue-i18n";
import { LanguageUtil } from "@coscine/app-util";

Vue.config.productionTip = false;
Vue.use(VueI18n);

jQuery(() => {
  const i18n = new VueI18n({
    locale: LanguageUtil.getLanguage(),
    messages: coscine.i18n.resources,
    silentFallbackWarn: true,
  });

  new Vue({
    render: (h) => h(ResourcesApp),
    i18n,
  }).$mount("resources");
});
